
// var WXBizDataCrypt=require('utils/cryptojs/RdWXBizDataCrypt.js')
// var AppId = 
// var AppSecret = 

//var COM = require('utils/common.js')
App({

  globalData: {
    userId: false,
    openId: null,
    nickName: null,
    shopOpened: null,
    shopId: null,
    avatarUrl: null,
    targetShopId: null,
    country: null,
    provice: null,
    city: null,
    payments: [],
    shipAgents: [],
    shopParams: [],
    deposit: 0
  },

  onLaunch: function (e) {

    var self = this
    var timestamp = Date.parse(new Date());
    timestamp = timestamp / 1000;
    console.log('App Launch at: ' + timestamp);

    wx.getStorage(
      {
        key: "openId",
        success(res) {
          self.globalData.openId = res.data
          self.globalData.shopId = wx.getStorageSync('shopId')
          self.globalData.nickName = wx.getStorageSync('nickname') ? wx.getStorageSync('nickname') : null
          self.globalData.shopOpened = wx.getStorageSync('shopOpened')
          self.globalData.userId = wx.getStorageSync('userId')
          self.globalData.avatarUrl = wx.getStorageSync('avatarUrl')
          self.globalData.targetShopId = wx.getStorageSync('targetShopId')
          self.globalData.addressList = wx.getStorageSync("addressList")
        },
        fail() {

          wx.login({

            success: function (res) {
              console.log("login ok")
              //self.setuserinfo(res.code)

            },
            fail: function (res) {
              console.log(res)
            }
          })
        }
      })
  },
  //用户登录后把用户储存在user表里, 把用户是否注册状态存入缓存
  //   saveOrUserData: function (userInfo) {
  //     var self = this


  // 		let url = COM.load('CON').CREATE_OR_UPDATE_USER;
  // 		COM.load('NetUtil').netUtil(url, "POST", { "open_id": self.globalData.openId, "name": userInfo.nickName, "avatarUrl": userInfo.avatarUrl, "country":userInfo.country, "province": userInfo.province,	"city": userInfo.city }, (callback) => {

  //       wx.setStorage({
  //         key: 'userId',
  //         data: callback,
  //       })
  //     })
  //   },

  //设定用户是否开过店以及商店ID
  
  jsonToMap: function (jsonStr) {
    return this.objToStrMap(JSON.parse(jsonStr));
  },
  objToStrMap: function (obj) {
    let strMap = new Map();
    for (let k of Object.keys(obj)) {
      strMap.set(k, obj[k]);
    }
    return strMap;
  },

  onShow: function () {
    console.log('App Show')


  },
  onHide: function () {
    console.log('App Hide')
  }
})
